# Requirements
1. Python >= 3.8
2. Windows 10

# Running the application
1. Clone this repository
2. Open the comand line or powershell and cd to the root folder
3. Run pip install -r requirements.txt
2. Run the create_exe.bat script
3. Navigate with windows explorer to the dist/TTS folder and open it
4. Search for the executable (.exe) file, and double click on it (it is advised to create a shortcut to this file)