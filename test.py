# %% [markdown]
# # Readme

# %% [markdown]
# ## Known issues
# - the path tts/en_US/by_book/female/judy_bieber/the_master_key/wavs/the_master_key_05_f000135.wav does not exist. The missing audio is present in tts/en_US/by_book/female/judy_bieber/the_master_key/wavs/the_master_key_05_f000136.wav. To fix this please move the text from the_master_key_05_f000135 to the_master_key_05_f000136 and remove the row with the_master_key_05_f000135 in metadata
# - the path tts/en_US/by_book/female/mary_ann/midnight_passenger/wavs/midnight_passenger_05_f000269.wav does not exist. It is simply not present so the fix for this is to remove this entry from the metadata
# - the path tts/en_US/by_book/female/mary_ann/northandsouth/wavs/northandsouth_40_f000069.wav does not exist. It is simply not present so the fix for this is to remove this entry from the metadata

# %% [markdown]
# # Setup for google drive

# %%
# !pip install autopep8
# !pip install eng-to-ipa
# !pip install pydot
# !pip install sklearn
# !pip install tensorflow_text
# !pip install librosa

# ROOT_PATH = '/content/drive/MyDrive'

# from google.colab import drive
# drive.mount('/content/drive')


# %% [markdown]
# # Setup for local pc

# %%
ROOT_PATH = '.'


# %% [markdown]
# # Global variables
# 

# %%
import numpy as np
from os.path import exists
from os import makedirs

TTS_FOLDER_PATH = f'{ROOT_PATH}/tts'
if not exists(TTS_FOLDER_PATH):
    makedirs(TTS_FOLDER_PATH)

SAVED_DATA = f'{TTS_FOLDER_PATH}/saved_data'
if not exists(SAVED_DATA):
    makedirs(SAVED_DATA)

AUDIO_PATH = f'{TTS_FOLDER_PATH}/generated_audio'
if not exists(AUDIO_PATH):
    makedirs(AUDIO_PATH)

MODEL_IMG_PATH = f'{TTS_FOLDER_PATH}/model_img'
if not exists(MODEL_IMG_PATH):
    makedirs(MODEL_IMG_PATH)

TENSORBOARD_LOG_PATH = f'{TTS_FOLDER_PATH}/tensorboard_logs'
if not exists(TENSORBOARD_LOG_PATH):
    makedirs(TENSORBOARD_LOG_PATH)

MODEL_CHECKPOINT_PATH = f'{TTS_FOLDER_PATH}/model_checkpoint'
if not exists(MODEL_CHECKPOINT_PATH):
    makedirs(MODEL_CHECKPOINT_PATH)


METADATA_FOLDERS = [
    # f'{TTS_FOLDER_PATH}/en_US/by_book/female/judy_bieber/dorothy_and_wizard_oz',
    # f'{TTS_FOLDER_PATH}/en_US/by_book/female/judy_bieber/emerald_city_of_oz',
    # f'{TTS_FOLDER_PATH}/en_US/by_book/female/judy_bieber/ozma_of_oz',
    # f'{TTS_FOLDER_PATH}/en_US/by_book/female/judy_bieber/rinkitink_in_oz',
    # f'{TTS_FOLDER_PATH}/en_US/by_book/female/judy_bieber/sky_island',
    # f'{TTS_FOLDER_PATH}/en_US/by_book/female/judy_bieber/the_master_key',
    # f'{TTS_FOLDER_PATH}/en_US/by_book/female/judy_bieber/the_sea_fairies',
    f'{TTS_FOLDER_PATH}/en_US/by_book/female/mary_ann/midnight_passenger',
    f'{TTS_FOLDER_PATH}/en_US/by_book/female/mary_ann/northandsouth',
    # f'{TTS_FOLDER_PATH}/en_US/by_book/male/elliot_miller/hunters_space',
    # f'{TTS_FOLDER_PATH}/en_US/by_book/male/elliot_miller/pink_fairy_book',
    # f'{TTS_FOLDER_PATH}/en_US/by_book/male/elliot_miller/pirates_of_ersatz',
    # f'{TTS_FOLDER_PATH}/en_US/by_book/male/elliot_miller/poisoned_pen',
    # f'{TTS_FOLDER_PATH}/en_US/by_book/male/elliot_miller/silent_bullet',
    # f'{TTS_FOLDER_PATH}/LJSpeech'
]


# There are 59394 data samples in the training set
MAX_DATA_BATCH_SIZE = 25

# This is the sampling rate for all audio files in the dataset
TARGET_SAMPLING_RATE = 2500

# The longest not trucnated audio file in the training set has less than 20 seconds.
MAX_AUDIO_SAMPLES_COUNT = 50000

# The longest words sequence has 59 words.
MAX_WORDS_COUNT = 100

# The longest phonemes sequence has 304 phonemes.
MAX_PHONEMES_COUNT = 400

# The longest char sequence has 331 characters.
MAX_CHAR_COUNT = 400


# %% [markdown]
# # I/O and utility functions

# %%
from typing import Tuple, List, Any
from keras.layers import Input, Layer, concatenate
from keras import Model
from keras.callbacks import ModelCheckpoint, TensorBoard
from keras.models import load_model
import tensorflow as tf
import pickle
import numpy as np
import pandas as pd
import librosa
import soundfile as sf


# read the audio file from the given path and return a spectrometer with trimmed silence periods
def read_audio_file(path_to_audio_file: str) -> np.ndarray:
    audio_data, samplerate = librosa.load(
        path=path_to_audio_file, sr=TARGET_SAMPLING_RATE)
    return np.trim_zeros(audio_data)


# save the audio file to the given path with a samplerate/2 silence at the start and end of file
def save_audio_file(path_to_audio_file: str, audio_data: np.ndarray) -> None:
    data = np.concatenate([
        np.zeros(int(TARGET_SAMPLING_RATE / 2)),
        np.trim_zeros(audio_data),
        np.zeros(int(TARGET_SAMPLING_RATE / 2))
    ])

    sf.write(path_to_audio_file, data, TARGET_SAMPLING_RATE)


# save data for the future to load it quicker
def save_data_frame(df: pd.DataFrame, path: str) -> None:
    with open(path, "wb") as f:
        pickle.dump(file=f, obj=df)


# load the previously saved data
def load_data_frame(path: str) -> pd.DataFrame:
    with open(path, "rb") as f:
        return pickle.load(file=f)


# creates the neural network from a nested list of layers and saves the model img to MODEL_IMG_PATH
def create_neural_network_model(name: str, input_shape: Tuple, layers: List[Any]) -> Model:
    if len(layers) < 1:
        raise ValueError('Invalid layers count')

    def layers_sequence(input_layer: Layer, layers: List[Any]) -> Layer:
        hidden_layer = input_layer
        i = 0
        while i < len(layers):
            if isinstance(layers[i], Layer):
                hidden_layer = layers[i](hidden_layer)
            elif isinstance(layers[i], tuple):
                merge_function = layers[i][0]
                joined_layers = [layers_sequence(
                    hidden_layer, seq) for seq in layers[i][1:]]
                hidden_layer = merge_function(joined_layers)
            else:
                raise ValueError('Invalid type in layers list')
            i += 1
        return hidden_layer

    input_layer = Input(shape=input_shape)
    output_layer = layers_sequence(input_layer, layers)

    model = Model(inputs=input_layer, outputs=output_layer, name=name)
    tf.keras.utils.plot_model(
        model, f'{MODEL_IMG_PATH}/{name}.png', show_shapes=True)
    model.summary()
    return model


# logs the current status of the network on a interactive web dashboard
def create_tensorboard_callback(name: str) -> Any:
    return TensorBoard(log_dir=f'{TENSORBOARD_LOG_PATH}/{name}'),


# saves the model progress every epoch
def create_model_checkpoint_callback(name: str) -> Any:
    return ModelCheckpoint(filepath=f'{MODEL_CHECKPOINT_PATH}/{name}', save_best_only=True)


# loads the model
def load_saved_model(name: str) -> Model:
    return load_model(filepath=f'{MODEL_CHECKPOINT_PATH}/{name}')


# %% [markdown]
# # Create data batches

# %%
import math
import numpy as np
import pandas as pd
from eng_to_ipa import convert
from sklearn.utils import shuffle
from typing import Callable


# reads all the data from a metadata folder
def get_metadata(metadata_folder: str) -> pd.DataFrame:
    df: pd.DataFrame = pd.read_csv(f'{metadata_folder}/metadata.csv', sep='|',
                                   header=None, names=['audio_file_name', 'original_text', 'cleaned_text'])

    df['cleaned_text'].fillna(df['original_text'], inplace=True)
    df['phonemes'] = [convert(text) for text in df['cleaned_text']]
    df['audio_data'] = [read_audio_file(f'{metadata_folder}/wavs/{wav_name}.wav')
                        for wav_name in df['audio_file_name']]
    return df


# creates a dataframe with all the data
def collect_all_metadata(load_previously_collected: bool = True) -> pd.DataFrame:
    if load_previously_collected:
        try:
            df = load_data_frame(f'{SAVED_DATA}/collected_data.P')
            print('data loaded from save file')
            return df
        except Exception:
            print('loading previously collected data failed')
    df_list = [get_metadata(folder) for folder in METADATA_FOLDERS]
    df = pd.concat(df_list, axis=0).reset_index(drop=True)
    save_data_frame(df, f'{SAVED_DATA}/collected_data.P')
    print('data collected and saved for future use')
    return df


# shuffles the data and iterates over it in batches invoking function on every batch
# this function is meant to remove the memory errors that happen due to large data sizes
def for_each_data_batch(df: pd.DataFrame, data_batch_size: int = MAX_DATA_BATCH_SIZE, function: Callable[[pd.DataFrame], None] = lambda df: print(df.shape)):
    data = shuffle(df).reset_index(drop=True)

    batches_count = math.ceil(data.shape[0] / data_batch_size)
    for i in range(1, batches_count):
        print(f'working on batch {i} out of {batches_count}')
        batch = data.iloc[data_batch_size * (i - 1): data_batch_size * i]
        function(batch.reset_index(drop=True))

    batch = data.iloc[data_batch_size * (batches_count - 1):]
    function(batch.reset_index(drop=True))


# %% [markdown]
# # Create the train/test sets

# %%
import numpy as np
import pandas as pd
from keras.preprocessing.text import Tokenizer
from tensorflow_text import UnicodeCharTokenizer
from keras.preprocessing.sequence import pad_sequences
from eng_to_ipa import convert
import numpy as np
from typing import List


# creates a tokenizer for phonemes
def create_phonemes_tokenizer() -> Tokenizer:
    # get a complete list of phonemes instead of doing this
    phonemes = collect_all_metadata()['phonemes'].to_list()

    tokenizer = Tokenizer(oov_token="<OOV>", char_level=True,
                          filters='#$%&*+-/<=>@[\\]^_`{|}~\t\n')
    tokenizer.fit_on_texts(phonemes)

    return tokenizer


# creates a tokenizer for words
def create_words_tokenizer() -> Tokenizer:
    with open(f'{TTS_FOLDER_PATH}/words_alpha.txt', 'rt') as words_file:
        # words from words list
        words_list = list(words_file.read().split())
        # whitespace and punctuation
        punctuation = [' ', ',', '.', '!', '?', ':', ';', '\'', '"', '(', ')']
        # sentences from dataset
        dataset_sentences = collect_all_metadata()['cleaned_text'].to_list()

        tokenizer = Tokenizer(
            oov_token="<OOV>", filters='#$%&*+-/<=>@[\\]^_`{|}~\t\n')
        tokenizer.fit_on_texts(words_list + punctuation + dataset_sentences)

        return tokenizer


# creates a tokenizer for unicode characters
def create_char_tokenizer() -> UnicodeCharTokenizer:
    return UnicodeCharTokenizer()


# removes all non ASCII character codes
def remove_non_ASCII_chars(unicode_char_sequences: List[List[int]]) -> List[List[int]]:
    return [list(filter(lambda unicode: unicode > 31 and unicode < 127, sequence))
            for sequence in unicode_char_sequences]


# converts the text in this batch into normalized float tokens
def preproces_texts(data_batch: pd.DataFrame):
    text_tokenizer = create_char_tokenizer()
    text_scaler = MinMaxScaler()
    text_scaler.fit([np.ones(MAX_CHAR_COUNT) * 32, np.ones(MAX_CHAR_COUNT) * 126])

    texts = data_batch['cleaned_text'].to_list()
    texts = text_tokenizer.tokenize(texts).to_list()
    texts = remove_non_ASCII_chars(texts)
    texts = pad_sequences(sequences=texts, maxlen=MAX_CHAR_COUNT,
                          value=32, padding='post')
    texts = text_scaler.transform(texts)
    return texts


# normalizes the audio data into float tokens
def preproces_audio(data_batch: pd.DataFrame):
    audio_scaler = MinMaxScaler()
    audio_scaler.fit([np.ones(MAX_AUDIO_SAMPLES_COUNT), np.ones(MAX_AUDIO_SAMPLES_COUNT) * -1])

    audio = data_batch['audio_data'].to_list()
    audio = pad_sequences(sequences=audio, maxlen=MAX_AUDIO_SAMPLES_COUNT,
                          value=0, padding='post')
    audio = audio_scaler.transform(audio)
    return audio


# %% [markdown]
# # Building models

# %%
from keras.layers import Dense, LSTM, Reshape, Conv1D, Add
from keras.losses import BinaryCrossentropy


generator = create_neural_network_model(name='generator', input_shape=(MAX_CHAR_COUNT), layers=[
    Reshape(target_shape=(MAX_CHAR_COUNT, 1)),
    LSTM(100, return_sequences=False, recurrent_activation='sigmoid',
         recurrent_dropout=0, unroll=False, use_bias=True),
    Dense(500, activation='tanh'),
    Dense(MAX_AUDIO_SAMPLES_COUNT, activation=None)
])
generator.compile(optimizer='adam', loss='mse')


discriminator = create_neural_network_model(name='discriminator', input_shape=(MAX_AUDIO_SAMPLES_COUNT), layers=[
    Reshape(target_shape=(MAX_AUDIO_SAMPLES_COUNT, 1)),
    (
        Add(),
        [
            Conv1D(1, 51, activation='relu'),
            Reshape(target_shape=[MAX_AUDIO_SAMPLES_COUNT - 50]),
            Dense(100, activation='tanh'),
        ],
        [
            Conv1D(1, 101, activation='relu'),
            Reshape(target_shape=[MAX_AUDIO_SAMPLES_COUNT - 100]),
            Dense(100, activation='tanh'),
        ],
        [
            Conv1D(1, 501, activation='relu'),
            Reshape(target_shape=[MAX_AUDIO_SAMPLES_COUNT - 500]),
            Dense(100, activation='tanh'),
        ],
        [
            Conv1D(1, 1001, activation='relu'),
            Reshape(target_shape=[MAX_AUDIO_SAMPLES_COUNT - 1000]),
            Dense(100, activation='tanh'),
        ],
    ),
    Dense(100, activation='tanh'),
    Dense(1, activation='sigmoid')
])
discriminator.compile(optimizer='adam', metrics=['accuracy'],
                      loss=BinaryCrossentropy(from_logits=False))


tts_gan=create_neural_network_model('tts_gan', input_shape=(MAX_CHAR_COUNT), layers=[
    generator,
    discriminator
])
tts_gan.compile(optimizer='adam', metrics=['accuracy'],
            loss=BinaryCrossentropy(from_logits=False))

# %% [markdown]
# # Train models

# %%
from keras import Model
import pandas as pd
from sklearn.preprocessing import MinMaxScaler


def print_step(step_message: str):
    print('============================================================================================')
    print(step_message)


def train_generator(texts: np.ndarray, audio: np.ndarray):
    generator.fit(texts, audio, batch_size=10,
                  epochs=10, validation_split=0.1, callbacks=[
                      create_model_checkpoint_callback(generator.name),
                      create_tensorboard_callback(generator.name)
                  ])


def train_discriminator(audio: np.ndarray, generator_output: np.ndarray):
    X_data = np.concatenate((audio, generator_output))
    y_data = np.concatenate((np.ones(audio.shape[0]),
                             np.zeros(generator_output.shape[0])))

    discriminator.fit(X_data, y_data, batch_size=10,
                      epochs=10, validation_split=0.1, callbacks=[
                          create_model_checkpoint_callback(discriminator.name),
                          create_tensorboard_callback(discriminator.name)
                      ])


def train_GAN(texts: np.ndarray):
    tts_gan.fit(texts, np.ones(texts.shape[0]), batch_size=10,
                epochs=10, validation_split=0.1, callbacks=[
                create_model_checkpoint_callback(tts_gan.name),
                create_tensorboard_callback(tts_gan.name)
                ])


def train_models(texts: np.ndarray, audio: np.ndarray):
    print_step('generating audio from texts')
    generated = generator.predict(texts)

    print_step('discriminator training start...')
    discriminator.trainable = True
    train_discriminator(audio=audio, generator_output=generated)

    print_step('GAN training start...')
    discriminator.trainable = False
    train_GAN(texts=texts)


def pre_train(texts: np.ndarray, audio: np.ndarray):
    print_step('generator training start...')
    train_generator(texts=texts, audio=audio)

    train_models(texts=texts, audio=audio)


# %%
df: pd.DataFrame = collect_all_metadata()


# %%
def train_on_batch(data_batch: pd.DataFrame):
    print_step('preprocessing texts')
    texts = preproces_texts(data_batch=data_batch)
    print_step('preprocessing audio')
    audio = preproces_audio(data_batch=data_batch)

    train_models(texts=texts, audio=audio)

# for _ in range(25):
for_each_data_batch(df=df, function=train_on_batch)
    


