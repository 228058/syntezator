pyinstaller^
 --onedir --noconfirm --noconsole --name TTS GUI.py^
 --add-data "./venv/Lib/site-packages/librosa/util/example_data/registry.txt;librosa/util/example_data"^
 --add-data "./venv/Lib/site-packages/librosa/util/example_data/index.json;librosa/util/example_data"^
 --add-data "./venv/Lib/site-packages/eng_to_ipa/resources/phones.json;eng_to_ipa/resources"^
 --add-data "./venv/Lib/site-packages/eng_to_ipa/resources/CMU_dict.json;eng_to_ipa/resources"^
 --add-data "./venv/Lib/site-packages/eng_to_ipa/resources/CMU_dict.db;eng_to_ipa/resources"^
 --collect-all "tensorflow_text"^
 --collect-submodules "sklearn"
 
xcopy ffmpeg\ dist\TTS\ffmpeg /E/H/Y/I
echo f | xcopy tts\model_checkpoint\checkpoint dist\TTS\tts\model_checkpoint\checkpoint /E/H/Y/I
echo f | xcopy tts\model_checkpoint\ckpt-75.index dist\TTS\tts\model_checkpoint\ckpt-75.index /E/H/Y/I
echo f | xcopy tts\model_checkpoint\ckpt-75.data-00000-of-00001 dist\TTS\tts\model_checkpoint\ckpt-75.data-00000-of-00001 /E/H/Y/I