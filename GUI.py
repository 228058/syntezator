from __future__ import annotations
from ast import Raise
from playsound import playsound
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtGui import QFont, QIcon
from PyQt5.QtWidgets import *
import matplotlib.figure as mpl_fig
from matplotlib.backends.backend_qt5agg import FigureCanvas
from matplotlib.backends.qt_compat import QtWidgets
from threading import Thread

from typing import *
import sys
import matplotlib
import shutil
import librosa
import warnings
import contextlib
import os

import main

matplotlib.use('Qt5Agg')
warnings.filterwarnings("ignore")

class ApplicationWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()

        # 1. Window settings
        self.setGeometry(100, 300, 800, 400)
        self.setWindowTitle("TTS")
        self.frm = QtWidgets.QFrame(self)
        self.frm.setStyleSheet("QWidget { background-color: #eeeeec; }")
        self.lyt = QtWidgets.QVBoxLayout()
        self.frm.setLayout(self.lyt)
        self.setCentralWidget(self.frm)
        self.counter = 0

        self.buttons = QGridLayout()
        self.Tbuttons = QVBoxLayout()
        self.Tfigure = QVBoxLayout()
        self.textbox = QTextEdit()

        self.engine = 1
        self.engineName=['GTTS', 'STTS']

        self.b0 = QPushButton("Prosess text")
        self.b1 = QPushButton(str("Change engine to \""+self.engineName[self.engine]+"\""))
        self.b2 = QPushButton("Save file")
        self.b3 = QPushButton("Play sound")

        self.myFig = MplCanvas()

        self.b0.clicked.connect(self.processText)
        self.b1.clicked.connect(self.switchEngine)
        self.b2.clicked.connect(self.saveFileNameDialog)
        self.b3.clicked.connect(self.playSound)

        self.buttons.addWidget(self.b0, 0, 0)
        self.buttons.addWidget(self.b1, 0, 1)
        self.buttons.addWidget(self.b2, 0, 2)
        self.buttons.addWidget(self.b3, 0, 3)

        self.Tbuttons.addWidget(self.textbox)
        self.Tbuttons.addLayout(self.buttons)
        self.Tfigure.addLayout(self.Tbuttons)
        self.Tfigure.addWidget(self.myFig)
        self.lyt.addLayout(self.Tfigure)

        self.signal = []
        self.text = ''
        self.tempFile = ''

        self.set_play_save_buttons_enabled(False)
        self.show()

    def closeEvent(self, event):
        raise RuntimeError("Application closed")

    def tempfile_name(self):
        return f'{main.AUDIO_PATH}/tempfile_{self.counter}.{"mp3" if self.engine % 2 else "wav"}'

    def set_play_save_buttons_enabled(self, enabled: bool):
        self.b2.setEnabled(enabled)
        self.b3.setEnabled(enabled)

    def set_all_buttons_enabled(self, enabled: bool): 
        self.b0.setEnabled(enabled)
        self.b1.setEnabled(enabled)
        self.b2.setEnabled(enabled)
        self.b3.setEnabled(enabled)

    def playSound(self):
        def _play(string: str):
            try:
                playsound(string)
                self.set_all_buttons_enabled(True)
            except Exception:
                self.set_all_buttons_enabled(False)

        self.set_all_buttons_enabled(False)
        self.play_thread = Thread(target=_play, args=(self.tempfile_name(),))
        self.play_thread.start()

    def processText(self):
        self.text = self.textbox.toPlainText()
        if self.text == '': return
        self.counter += 1
        if self.engine % 2:
            self.signal = main.generateGTTS(self.text, self.counter)
        else:
            self.signal = main.generateSTTS(75, self.text, self.counter)
            self.signal, sr = librosa.load(self.tempfile_name())

        self.set_play_save_buttons_enabled(True)
        self.update_plot()

    def switchEngine(self):
        self.engine += 1
        self.engine %= 2
        self.b1.setText(str("Change engine to \""+self.engineName[self.engine]+"\""))
        self.set_play_save_buttons_enabled(False)

    def saveFileNameDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        ext = ".mp3" if self.engine % 2 else ".wav"
        fileName, _ = QFileDialog.getSaveFileName(
            self, "save file", ext, f"sound file(*{ext});;All Files(*)", options=options)
        if fileName:
            try:
                shutil.copy(self.tempfile_name(), fileName)
            except Exception:
                print("Error occurred while copying file.")

    def update_plot(self):
        self.xdata = list(range(len(self.signal)))
        self.ydata = self.signal
        self.myFig.axes.cla()  # Clear the canvas.
        self.myFig.axes.plot(self.xdata, self.ydata, 'r')
        self.myFig.draw()


class MplCanvas(FigureCanvas):
    def __init__(self, parent=None, width=240, height=30, dpi=10, frameon=False):
        fig = mpl_fig.Figure(figsize=(width, height), dpi=dpi, linewidth=0.2)
        self.axes = fig.add_subplot()
        super(MplCanvas, self).__init__(fig)


if __name__ == "__main__":
    this_file_path = os.path.dirname(__file__)
    ffmpeg_path = os.path.join(this_file_path, 'ffmpeg')
    ffmpeg_bin_path = os.path.join(ffmpeg_path, 'bin')
    print(ffmpeg_bin_path)

    os.environ["PATH"] += os.pathsep + ffmpeg_bin_path
    
    qapp = QtWidgets.QApplication(sys.argv)
    app = ApplicationWindow()
    with contextlib.suppress(RuntimeError):
        qapp.exec_()