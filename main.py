# %% [markdown]
#  # Readme

# %% [markdown]
#  ## Known issues
#  - the path tts/en_US/by_book/female/judy_bieber/the_master_key/wavs/the_master_key_05_f000135.wav does not exist. The missing audio is present in tts/en_US/by_book/female/judy_bieber/the_master_key/wavs/the_master_key_05_f000136.wav. To fix this please move the text from the_master_key_05_f000135 to the_master_key_05_f000136 and remove the row with the_master_key_05_f000135 in metadata
#  - the path tts/en_US/by_book/female/mary_ann/midnight_passenger/wavs/midnight_passenger_05_f000269.wav does not exist. It is simply not present so the fix for this is to remove this entry from the metadata
#  - the path tts/en_US/by_book/female/mary_ann/northandsouth/wavs/northandsouth_40_f000069.wav does not exist. It is simply not present so the fix for this is to remove this entry from the metadata

# %% [markdown]
#  # Setup for google drive

# %%
# !pip install autopep8
# !pip install eng-to-ipa
# !pip install pydot
# !pip install sklearn
# !pip install tensorflow_text
# !pip install librosa

# ROOT_PATH = '/content/drive/MyDrive'

# from google.colab import drive
# drive.mount('/content/drive')

# %% [markdown]
#  # Setup for local pc

# %%
ROOT_PATH = '.'

# %% [markdown]
#  # Global variables
#

# %%
import numpy as np
from os.path import exists
from os import makedirs
import sys

TTS_FOLDER_PATH = f'{ROOT_PATH}/tts'
SAVED_DATA = f'{TTS_FOLDER_PATH}/saved_data'
AUDIO_PATH = f'{TTS_FOLDER_PATH}/generated_audio'
MODEL_IMG_PATH = f'{TTS_FOLDER_PATH}/model_img'
TENSORBOARD_LOG_PATH = f'{TTS_FOLDER_PATH}/tensorboard_logs'
MODEL_CHECKPOINT_PATH = f'{TTS_FOLDER_PATH}/model_checkpoint'

METADATA_FOLDERS = [
    f'{TTS_FOLDER_PATH}/en_US/by_book/female/judy_bieber/dorothy_and_wizard_oz',
    f'{TTS_FOLDER_PATH}/en_US/by_book/female/judy_bieber/emerald_city_of_oz',
    f'{TTS_FOLDER_PATH}/en_US/by_book/female/judy_bieber/ozma_of_oz',
    f'{TTS_FOLDER_PATH}/en_US/by_book/female/judy_bieber/rinkitink_in_oz',
    f'{TTS_FOLDER_PATH}/en_US/by_book/female/judy_bieber/sky_island',
    f'{TTS_FOLDER_PATH}/en_US/by_book/female/judy_bieber/the_master_key',
    f'{TTS_FOLDER_PATH}/en_US/by_book/female/judy_bieber/the_sea_fairies',
    f'{TTS_FOLDER_PATH}/en_US/by_book/female/mary_ann/midnight_passenger',
    f'{TTS_FOLDER_PATH}/en_US/by_book/female/mary_ann/northandsouth',
    f'{TTS_FOLDER_PATH}/en_US/by_book/male/elliot_miller/hunters_space',
    f'{TTS_FOLDER_PATH}/en_US/by_book/male/elliot_miller/pink_fairy_book',
    f'{TTS_FOLDER_PATH}/en_US/by_book/male/elliot_miller/pirates_of_ersatz',
    f'{TTS_FOLDER_PATH}/en_US/by_book/male/elliot_miller/poisoned_pen',
    f'{TTS_FOLDER_PATH}/en_US/by_book/male/elliot_miller/silent_bullet',
    f'{TTS_FOLDER_PATH}/LJSpeech'
]

TEST_DICT = dict({
    'i_like_trains': 'i like trains',
    'this_is_a_test': 'test, test, test, one, two, three, this is a test text',
})

# There are 59394 data samples in the training set
MAX_DATA_BATCH_SIZE = 100

# This is the sampling rate for all audio files in the dataset
TARGET_SAMPLING_RATE = 8000

# The longest not trucnated audio file in the training set has less than 20 seconds.
MAX_AUDIO_SAMPLES_COUNT = 160000

# The longest words sequence has 59 words.
MAX_WORDS_COUNT = 100

# The longest phonemes sequence has 304 phonemes.
MAX_PHONEMES_COUNT = 400

# The longest char sequence has 331 characters.
MAX_CHAR_COUNT = 400

# %% [markdown]
#  # I/O and utility functions

# %%
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from typing import Tuple, List, Any
from keras.layers import Input, Layer, concatenate
from keras import Model
from keras.callbacks import ModelCheckpoint, TensorBoard
from keras.models import load_model
import tensorflow as tf
import pickle
import numpy as np
import pandas as pd
import librosa
import soundfile as sf

# create the folders if this is a first run in dev mode

def make_folders():
    if not exists(TTS_FOLDER_PATH):
        makedirs(TTS_FOLDER_PATH)
    if not exists(SAVED_DATA):
        makedirs(SAVED_DATA)
    if not exists(AUDIO_PATH):
        makedirs(AUDIO_PATH)
    if not exists(MODEL_IMG_PATH):
        makedirs(MODEL_IMG_PATH)
    if not exists(TENSORBOARD_LOG_PATH):
        makedirs(TENSORBOARD_LOG_PATH)
    if not exists(MODEL_CHECKPOINT_PATH):
        makedirs(MODEL_CHECKPOINT_PATH)

# make_folders()

# read the audio file from the given path and return a spectrometer with trimmed silence periods

def read_audio_file(path_to_audio_file: str) -> np.ndarray:
    audio_data, samplerate = librosa.load(
        path=path_to_audio_file, sr=TARGET_SAMPLING_RATE)
    return np.trim_zeros(audio_data)

# save the audio file to the given path with a samplerate/2 silence at the start and end of file

def save_audio_file(path_to_audio_file: str, audio_data: np.ndarray) -> None:
    data = np.concatenate([
        np.zeros(int(TARGET_SAMPLING_RATE / 2)),
        np.trim_zeros(audio_data),
        np.zeros(int(TARGET_SAMPLING_RATE / 2))
    ])

    sf.write(path_to_audio_file, data, TARGET_SAMPLING_RATE)

# save data for the future to load it quicker

def save_data_frame(df: pd.DataFrame, path: str) -> None:
    with open(path, "wb") as f:
        pickle.dump(file=f, obj=df)

# load the previously saved data

def load_data_frame(path: str) -> pd.DataFrame:
    with open(path, "rb") as f:
        return pickle.load(file=f)

# creates the neural network from a nested list of layers and saves the model img to MODEL_IMG_PATH

def create_neural_network_model(name: str, input_shape: Tuple, layers: List[Any]) -> Model:
    if len(layers) < 1:
        raise ValueError('Invalid layers count')

    def layers_sequence(input_layer: Layer, layers: List[Any]) -> Layer:
        hidden_layer = input_layer
        i = 0
        while i < len(layers):
            print(f'adding layer {i}')
            if isinstance(layers[i], Layer):
                hidden_layer = layers[i](hidden_layer)
            elif isinstance(layers[i], tuple):
                merge_function = layers[i][0]
                joined_layers = [layers_sequence(
                    hidden_layer, seq) for seq in layers[i][1:]]
                hidden_layer = merge_function(joined_layers)
            else:
                raise ValueError('Invalid type in layers list')
            i += 1
        return hidden_layer

    input_layer = Input(shape=input_shape)
    output_layer = layers_sequence(input_layer, layers)

    model = Model(inputs=input_layer, outputs=output_layer, name=name)
    return model

# # logs the current status of the network on a interactive web dashboard
# def create_tensorboard_callback(name: str) -> Any:
#     return TensorBoard(log_dir=f'{TENSORBOARD_LOG_PATH}/{name}'),

# # saves the model progress every epoch
# def create_model_checkpoint_callback(name: str) -> Any:
#     return ModelCheckpoint(filepath=f'{MODEL_CHECKPOINT_PATH}/{name}', save_best_only=True)

# # loads the model
# def load_saved_model(name: str) -> Model:
#     return load_model(filepath=f'{MODEL_CHECKPOINT_PATH}/{name}')

# %% [markdown]
#  # Create data batches

# %%
import math
import numpy as np
import pandas as pd
from eng_to_ipa import convert
from sklearn.utils import shuffle
from typing import Callable

# reads all the data from a metadata folder

def get_metadata(metadata_folder: str) -> pd.DataFrame:
    df: pd.DataFrame = pd.read_csv(f'{metadata_folder}/metadata.csv', sep='|',
                                    header=None, names=['audio_file_name', 'original_text', 'cleaned_text'])

    df['cleaned_text'].fillna(df['original_text'], inplace=True)
    df['phonemes'] = [convert(text) for text in df['cleaned_text']]
    df['audio_data'] = [read_audio_file(f'{metadata_folder}/wavs/{wav_name}.wav')
                        for wav_name in df['audio_file_name']]
    return df

# creates a dataframe with all the data

def collect_all_metadata(load_previously_collected: bool = True, filter: str = None) -> pd.DataFrame:
    df = None
    if load_previously_collected:
        try:
            df = load_data_frame(f'{SAVED_DATA}/collected_data.P')
            print('data loaded from save file')
        except Exception:
            load_previously_collected = False
            print('loading previously collected data failed')

    if not load_previously_collected:
        df_list = [get_metadata(folder) for folder in METADATA_FOLDERS]
        df = pd.concat(df_list, axis=0).reset_index(drop=True)
        save_data_frame(df, f'{SAVED_DATA}/collected_data.P')
        print('data collected and saved for future use')

    if filter is not None:
        df = df[df['audio_file_name'].str.contains(
            '|'.join(filter))].reset_index(drop=True)

    return df

# shuffles the data and iterates over it in batches invoking function on every batch
# this function is meant to remove the memory errors that happen due to large data sizes

def for_each_data_batch(df: pd.DataFrame, data_batch_size: int = MAX_DATA_BATCH_SIZE, function: Callable[[pd.DataFrame], None] = lambda df: print(df.shape)):
    data = shuffle(df).reset_index(drop=True)

    batches_count = math.ceil(data.shape[0] / data_batch_size)
    for i in range(1, batches_count):
        print(f'working on batch {i} out of {batches_count}')
        batch = data.iloc[data_batch_size * (i - 1): data_batch_size * i]
        function(batch.reset_index(drop=True))

    print(f'working on batch {batches_count} out of {batches_count}')
    batch = data.iloc[data_batch_size * (batches_count - 1):]
    function(batch.reset_index(drop=True))

# %% [markdown]
#  # Create the train/test sets

# %%
import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from keras.preprocessing.text import Tokenizer
from tensorflow_text import UnicodeCharTokenizer
from keras_preprocessing.sequence import pad_sequences
from eng_to_ipa import convert
import numpy as np
from typing import Tuple, List, Any
import os

# creates a tokenizer for phonemes

def create_phonemes_tokenizer() -> Tokenizer:
    # get a complete list of phonemes instead of doing this
    phonemes = collect_all_metadata()['phonemes'].to_list()

    tokenizer = Tokenizer(oov_token="<OOV>", char_level=True,
                            filters='#$%&*+-/<=>@[\\]^_`{|}~\t\n')
    tokenizer.fit_on_texts(phonemes)

    return tokenizer

# creates a tokenizer for words

def create_words_tokenizer() -> Tokenizer:
    with open(f'{TTS_FOLDER_PATH}/words_alpha.txt', 'rt') as words_file:
        # words from words list
        words_list = list(words_file.read().split())
        # whitespace and punctuation
        punctuation = [' ', ',', '.', '!',
                        '?', ':', ';', '\'', '"', '(', ')']
        # sentences from dataset
        dataset_sentences = collect_all_metadata()[
            'cleaned_text'].to_list()

        tokenizer = Tokenizer(
            oov_token="<OOV>", filters='#$%&*+-/<=>@[\\]^_`{|}~\t\n')
        tokenizer.fit_on_texts(
            words_list + punctuation + dataset_sentences)

        return tokenizer

# creates a tokenizer for unicode characters

def create_char_tokenizer() -> UnicodeCharTokenizer:
    return UnicodeCharTokenizer()

# removes all non ASCII character codes

def remove_non_ASCII_chars(unicode_char_sequences: List[List[int]]) -> List[List[int]]:
    return [list(filter(lambda unicode: unicode > 31 and unicode < 127, sequence))
            for sequence in unicode_char_sequences]

# converts the text in this batch into normalized float tokens

def preproces_texts(texts: List[str]):
    text_tokenizer = create_char_tokenizer()
    text_scaler = MinMaxScaler()
    text_scaler.fit([np.ones(MAX_CHAR_COUNT) * 32,
                    np.ones(MAX_CHAR_COUNT) * 126])

    texts = text_tokenizer.tokenize(texts).to_list()
    texts = remove_non_ASCII_chars(texts)
    texts = pad_sequences(sequences=texts, maxlen=MAX_CHAR_COUNT,
                            value=32, padding='post')
    texts = text_scaler.transform(texts)
    return texts

# normalizes the audio data into float tokens

def preproces_audio(audio: List[np.ndarray]):
    audio = pad_sequences(sequences=audio, maxlen=MAX_AUDIO_SAMPLES_COUNT,
                            dtype='float32', value=0, padding='post')
    return audio

# %% [markdown]
#  # Building the model

# %%
from keras.layers import Dense, LSTM, Reshape, Conv1D, Add, Concatenate, LeakyReLU, Dropout, Flatten, Conv1DTranspose, AveragePooling1D, MaxPooling1D, BatchNormalization, UpSampling1D
from keras.losses import MeanSquaredError, BinaryCrossentropy
from keras.initializers import RandomNormal
import numpy as np
import sys

initializer = RandomNormal(mean=0., stddev=1.)

generator_layers = [
    Dense(625 * 256, use_bias=True,
            kernel_initializer=initializer),  # 160000 = 160000
    BatchNormalization(),
    LeakyReLU(),

    Reshape((625, 256)),  # 625, 256 = 160000

    Conv1DTranspose(filters=128, kernel_size=4, strides=2, padding='same',
                    use_bias=False, kernel_initializer=initializer),  # 1250, 128 = 160000
    BatchNormalization(),
    LeakyReLU(),

    Conv1DTranspose(filters=64, kernel_size=4, strides=2, padding='same',
                    use_bias=False, kernel_initializer=initializer),  # 2500, 64 = 160000
    BatchNormalization(),
    LeakyReLU(),

    Conv1DTranspose(filters=16, kernel_size=16, strides=4, padding='same',
                    use_bias=False, kernel_initializer=initializer),  # 10000, 16 = 160000
    BatchNormalization(),
    LeakyReLU(),

    Conv1DTranspose(filters=4, kernel_size=16, strides=4, padding='same',
                    use_bias=False, kernel_initializer=initializer),  # 40000, 4 = 160000
    BatchNormalization(),
    LeakyReLU(),

    Conv1DTranspose(filters=1, kernel_size=16, strides=4, padding='same',
                    use_bias=False, kernel_initializer=initializer),  # 160000, 1 = 160000
    BatchNormalization(),
    LeakyReLU(),

    Reshape([MAX_AUDIO_SAMPLES_COUNT]),  # 160000 = 160000
]

discriminator_layers = [
    Reshape((MAX_AUDIO_SAMPLES_COUNT, 1)),  # 160000, 1

    Conv1D(filters=16, kernel_size=4, strides=2, padding='same',
            use_bias=False, kernel_initializer=initializer),  # 80000, 256
    BatchNormalization(),
    LeakyReLU(alpha=0.2),

    Conv1D(filters=16, kernel_size=4, strides=2, padding='same',
            use_bias=False, kernel_initializer=initializer),  # 40000, 256
    BatchNormalization(),
    LeakyReLU(alpha=0.2),

    Conv1D(filters=16, kernel_size=4, strides=2, padding='same',
            use_bias=False, kernel_initializer=initializer),  # 20000, 256
    BatchNormalization(),
    LeakyReLU(alpha=0.2),

    Conv1D(filters=16, kernel_size=4, strides=2, padding='same',
            use_bias=False, kernel_initializer=initializer),  # 10000, 256
    BatchNormalization(),
    LeakyReLU(alpha=0.2),

    Conv1D(filters=32, kernel_size=4, strides=2, padding='same',
            use_bias=False, kernel_initializer=initializer),  # 5000, 256
    BatchNormalization(),
    LeakyReLU(alpha=0.2),

    Conv1D(filters=32, kernel_size=4, strides=2, padding='same',
            use_bias=False, kernel_initializer=initializer),  # 2500, 256
    BatchNormalization(),
    LeakyReLU(alpha=0.2),

    Conv1D(filters=64, kernel_size=4, strides=2, padding='same',
            use_bias=False, kernel_initializer=initializer),  # 1250, 256
    BatchNormalization(),
    LeakyReLU(alpha=0.2),

    Conv1D(filters=64, kernel_size=4, strides=2, padding='same',
            use_bias=False, kernel_initializer=initializer),  # 625, 256
    BatchNormalization(),
    LeakyReLU(alpha=0.2),

    Flatten(),  # 800000
    Dropout(0.1),

    Dense(1, activation='sigmoid', kernel_initializer=initializer)  # 1
]

generator = create_neural_network_model(
    'generator', (MAX_CHAR_COUNT), generator_layers)
generator_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)

discriminator = create_neural_network_model(
    'discriminator', (MAX_AUDIO_SAMPLES_COUNT), discriminator_layers)
discriminator_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)

checkpoint = tf.train.Checkpoint(generator=generator,
                                    generator_optimizer=generator_optimizer,
                                    discriminator=discriminator,
                                    discriminator_optimizer=discriminator_optimizer)

def save_models():
    checkpoint.save(f'{MODEL_CHECKPOINT_PATH}/ckpt')

def load_models(epoch: int):
    checkpoint.restore(f'{MODEL_CHECKPOINT_PATH}/ckpt-{epoch}')

# %% [markdown]
#  # Train the model

# %%
import time
from keras.layers import Reshape

import tensorflow as tf

def discriminator_loss(real_output, fake_output):
    real_loss = BinaryCrossentropy()(tf.ones_like(real_output), real_output)
    fake_loss = BinaryCrossentropy()(tf.zeros_like(fake_output), fake_output)
    return real_loss + fake_loss

def discriminator_acc(real_output, fake_output):
    real_acc = (tf.reduce_sum(tf.round(real_output))) / MAX_DATA_BATCH_SIZE
    fake_acc = (MAX_DATA_BATCH_SIZE -
                tf.reduce_sum(tf.round(fake_output))) / MAX_DATA_BATCH_SIZE
    return real_acc, fake_acc, (real_acc + fake_acc) / 2

def generator_loss(fake_output):
    return BinaryCrossentropy()(tf.ones_like(fake_output), fake_output)

def generator_loss_pre_train(real_data, generated_data):
    return MeanSquaredError()(real_data, generated_data)

@tf.function
def pre_train_generator(texts: np.ndarray, audio: np.ndarray):
    with tf.GradientTape() as gen_tape:
        generated_audio = generator(texts, training=True)
        gen_loss = generator_loss_pre_train(audio, generated_audio)

        gradients_of_generator = gen_tape.gradient(
            gen_loss, generator.trainable_variables)

        generator_optimizer.apply_gradients(
            zip(gradients_of_generator, generator.trainable_variables))

@tf.function
def train_step(texts: np.ndarray, audio: np.ndarray):
    # tf.print('max_audio_value: ', tf.reduce_max(audio))
    # tf.print('min_audio_value: ', tf.reduce_min(audio))
    with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
        generated_audio = generator(texts, training=True)

        real_output = discriminator(audio, training=True)
        fake_output = discriminator(generated_audio, training=True)

        disc_loss = discriminator_loss(real_output, fake_output)
        gradients_of_discriminator = disc_tape.gradient(
            disc_loss, discriminator.trainable_variables)
        discriminator_optimizer.apply_gradients(
            zip(gradients_of_discriminator, discriminator.trainable_variables))

        disc_acc_real, disc_acc_fake, disc_acc_avg = discriminator_acc(
            real_output, fake_output)
        tf.print('disc_acc_real:', disc_acc_real)
        tf.print('disc_acc_fake:', disc_acc_fake)
        tf.print('disc_acc_avg:', disc_acc_avg)
        if disc_acc_avg > 0.85 and disc_acc_real > 0.85:
            tf.print('discriminator confidence high enough to train generator')
            gen_loss = generator_loss(fake_output)
            gradients_of_generator = gen_tape.gradient(
                gen_loss, generator.trainable_variables)
            generator_optimizer.apply_gradients(
                zip(gradients_of_generator, generator.trainable_variables))

def generate_and_save_audio(audio_file_names: List[str], preprocessed_texts: np.ndarray, suffix: str = None):
    generated_audio = generator(preprocessed_texts, training=False)

    names_count = len(audio_file_names)
    audio_count = generated_audio.shape[0]

    if (names_count != audio_count):
        raise ValueError('invalid audio file names count, \
                        got: {names_count}, expected: {audio_count}')

    for i in range(names_count):
        audio_path = f'{AUDIO_PATH}/{audio_file_names[i]}.wav'
        if suffix is not None:
            audio_path = f'{AUDIO_PATH}/{audio_file_names[i]}_{suffix}.wav'
        save_audio_file(audio_path, generated_audio[i])

    return generated_audio

def pretrain_on_batch(data_batch: pd.DataFrame):
    texts = data_batch['cleaned_text'].to_list()
    texts = preproces_texts(texts)

    audio = data_batch['audio_data'].to_list()
    audio = preproces_audio(audio)

    pre_train_generator(texts, audio)

def train_on_batch(data_batch: pd.DataFrame):
    texts = data_batch['cleaned_text'].to_list()
    texts = preproces_texts(texts)

    audio = data_batch['audio_data'].to_list()
    audio = preproces_audio(audio)

    train_step(texts, audio)

def train(dataset, epochs):
    for_each_data_batch(df=dataset, function=pretrain_on_batch)
    save_models()

    preprocessed_texts = preproces_texts(list(TEST_DICT.values()))
    generate_and_save_audio(list(TEST_DICT.keys()),
                            preprocessed_texts, str(0).zfill(3))

    for epoch in range(epochs):
        start = time.time()
        for_each_data_batch(df=dataset, function=train_on_batch)
        save_models()

        preprocessed_texts = preproces_texts(list(TEST_DICT.values()))
        generate_and_save_audio(
            list(TEST_DICT.keys()), preprocessed_texts, str(epoch + 1).zfill(3))

        print(f'Time for epoch {epoch + 1} is {time.time() - start} sec')

def continue_training(dataset, epochs, from_epoch):
    load_models(from_epoch)

    for epoch in range(from_epoch, from_epoch + epochs):
        start = time.time()
        for_each_data_batch(df=dataset, function=train_on_batch)
        save_models()

        preprocessed_texts = preproces_texts(list(TEST_DICT.values()))
        generate_and_save_audio(
            list(TEST_DICT.keys()), preprocessed_texts, str(epoch + 1).zfill(3))

        print(f'Time for epoch {epoch + 1} is {time.time() - start} sec')

# %% [markdown]
#  # Run in production mode

# %%
import os
import sys
import numpy as np
import librosa
import pandas as pd

def generateSTTS(last_model_epoch, text: str = None, counter: int = 0):
    load_models(last_model_epoch)
    preprocessed_text = preproces_texts([text])
    generated_audio = generator(preprocessed_text, training=False)[0]
    save_audio_file(f'{AUDIO_PATH}/tempfile_{counter}.wav', generated_audio)
    return generated_audio.numpy().tolist()

from gtts import gTTS
from pydub import AudioSegment

def generateGTTS(text: str = None, counter: int = 0):
    tts = gTTS(text)
    tts.save(f'{AUDIO_PATH}/tempfile_{counter}.mp3')
    audSeg = AudioSegment.from_mp3(f'{AUDIO_PATH}/tempfile_{counter}.mp3')
    return list(audSeg.get_array_of_samples())

#
#sound = AudioSegment.from_mp3("myfile.mp3")
#sound.export("myfile.wav", format="wav")
# import tempfile
# print("Creating a named temporary file..")
# temp = tempfile.NamedTemporaryFile()
# print("Created file is:", temp)
# print("Name of the file is:", temp.name)
# temp.close()

# %% [markdown]
#  # Testing

# %%
# df: pd.DataFrame = collect_all_metadata(filter=[
#     # 'dorothy_and_wizard_oz',
#     # 'emerald_city_of_oz',
#     # 'ozma_of_oz',
#     # 'rinkitink_in_oz',
#     # 'sky_island',
#     # 'the_master_key',
#     # 'the_sea_fairies',
#     'LJ001-'
# ])
# df: pd.DataFrame = collect_all_metadata()

# %%
# train(dataset=df, epochs=1000)
# continue_training(dataset=df, epochs=500, from_epoch=164)
# continue_training(dataset=df, epochs=500, from_epoch=75)

# %%

# if __name__ == '__main__':
#     try:
#         main()
#         input('finished')
#     except Exception as e:
#         enable_print()
#         print(e)
#         input('exception somewhere in main')

# %%
